serverTools created by TeaMRisk.

commands:
kick [player]
ban [player] [reason] [time]
seen [player]

formate:
time: 0y 0mo 0w 0d 0h 0m 0s

permissions:
tools.kick
tools.kick.notify
tools.ban
tools.ban.exempt
tools.ban.notify
tools.ban.temp
tools.ban.temp.max.#
tools.ban.ip
tools.ban.ip.notify
tools.ban.ip.temp
tools.ban.ip.temp.max.#
tools.seen
tools.seen.location
tools.seen.ip