package net.teamrisk.common;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by John on 1/18/2017.
 */
public class TimeBetween {

    private static int years;
    private static int months;
    private static int days;
    private static int hours;
    private static int minutes;
    private static int seconds;
    private static int millis;

    public static void get(LocalDateTime created, LocalDateTime expires) {
        LocalDateTime fromTemp = LocalDateTime.from(created);
        long lyears = fromTemp.until(expires, ChronoUnit.YEARS);
        fromTemp = fromTemp.plusYears(lyears);
        long lmonths = fromTemp.until(expires, ChronoUnit.MONTHS);
        fromTemp = fromTemp.plusMonths(lmonths);
        long ldays = fromTemp.until(expires, ChronoUnit.DAYS);
        fromTemp = fromTemp.plusDays(ldays);
        long lhours = fromTemp.until(expires, ChronoUnit.HOURS);
        fromTemp = fromTemp.plusHours(lhours);
        long lminutes = fromTemp.until(expires, ChronoUnit.MINUTES);
        fromTemp = fromTemp.plusMinutes(lminutes);
        long lseconds = fromTemp.until(expires, ChronoUnit.SECONDS);
        fromTemp = fromTemp.plusSeconds(lseconds);
        long millis = fromTemp.until(expires, ChronoUnit.MILLIS);

        years = Math.toIntExact(lyears);
        months = Math.toIntExact(lmonths);
        days = Math.toIntExact(ldays);
        hours = Math.toIntExact(lhours);
        minutes = Math.toIntExact(lminutes);
        seconds = Math.toIntExact(lseconds);
    }

    public static String getString(LocalDateTime created, LocalDateTime expires) {

        get(created, expires);
        String Time = "";

        if (years != 0) Time = Time + years +  " years ";
        if (months != 0) if (months <= 1) Time = Time + months + " month "; else Time = Time + months + " months ";
        if (days != 0) if (days <= 1) Time = Time + days + " day "; else Time = Time + days + " days ";
        if (hours != 0) Time = Time + hours + " hours ";
        if (minutes != 0) Time = Time + minutes + " minutes ";
        if (seconds != 0) Time = Time + seconds + " seconds";

        String bTime = years + "-" + months + "-" + days + " " + hours + ":" + minutes + ":" + seconds;
        return Time;
    }

    public static Integer getYears() {
        return Math.toIntExact(years);
    }
}
