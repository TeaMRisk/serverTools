package net.teamrisk.common;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

class FetchedUuid {
	String id;
}

public class UUIDFetcher {
	private static final String PROFILE_URL = "https://api.mojang.com/profiles/minecraft";
	private static HashMap<String, UUID> cache = new HashMap<String, UUID>();

	private static UUID fetch(String name) throws IOException {
		Gson gson = new GsonBuilder().create();
		UUID uuid = null;
		HttpURLConnection connection = createConnection();
		String body = gson.toJson(name);
		writeBody(connection, body);
		FetchedUuid[] id = gson.fromJson(new InputStreamReader(connection.getInputStream()), FetchedUuid[].class);
		if (id.length == 0)
			return null;
		uuid = UUIDFetcher.getUUIDFromString(id[0].id);
		cache.put(name.toLowerCase(), uuid);
		return uuid;
	}

	private static void writeBody(HttpURLConnection connection, String body) throws IOException {
		OutputStream stream = connection.getOutputStream();
		stream.write(body.getBytes());
		stream.flush();
		stream.close();
	}

	private static HttpURLConnection createConnection() throws IOException {
		URL url = new URL(PROFILE_URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		return connection;
	}

	public static UUID getUUIDFromString(String id) {
		return UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-"
				+ id.substring(16, 20) + "-" + id.substring(20, 32));
	}

	public static UUID getUUIDOf(String name) throws IOException {
		UUID uuid = cache.get(name.toLowerCase());
		if (uuid == null)
			uuid = fetch(name);
		return uuid;
	}
}