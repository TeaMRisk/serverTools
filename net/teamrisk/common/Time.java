package net.teamrisk.common;

import net.md_5.bungee.api.ChatColor;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Time {

	private static Pattern timePattern = Pattern.compile("(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*(?:s[a-z]*)?)?", Pattern.CASE_INSENSITIVE);

	public static String getOnlineTime(long t) {
		long time = System.currentTimeMillis();
		long timeOnFile = t;
		long timePlayed1 = time - timeOnFile;
		
		long timePlayed = timePlayed1/1000;
		int d = (int) (timePlayed / (86400));
		int h = (int) (timePlayed / (3600));
		int m = (int) ((timePlayed - (h * 3600)) / 60);
		int s = (int) (timePlayed - (h * 3600) - m * 60);
		String b = null;
		if (s != 0) {
			b = s + " seconds";
		}
		if (m != 0) {
			b = m + " minutes " + b;
		}
		if (h != 0) {
			b = h%24 + " hours " + b;
		}
		if (d != 0) {
			b = d + " days " + b;
		}
	    String timeOnline = ChatColor.RED + b.toString();
		return timeOnline;
	}

	public static String getCurrentTime() {
        LocalDateTime currentDate = LocalDateTime.now();
        String Stime = currentDate.toLocalDate() + " " + currentDate.toLocalTime().withNano(0);
        return Stime;
    }

    public static String getFormat(int milliseconds) {
	    LocalDateTime time = Instant.ofEpochMilli(milliseconds).atZone(ZoneId.systemDefault()).toLocalDateTime();
	    String sTime = time.toLocalDate() + " " + time.toLocalTime().withNano(0);
	    return sTime;
    }

    public static String fromMilliseconds(long milliseconds) {
        LocalDateTime time = Instant.ofEpochMilli(milliseconds).atZone(ZoneId.systemDefault()).toLocalDateTime();
        String sTime = time.toLocalDate() + " " + time.toLocalTime().withNano(0);
        return sTime;
    }

    public static LocalDateTime fromString(String localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(localDateTime, formatter);
        return dateTime;
    }

    public static String removeTimePattern(String input) {
        return timePattern.matcher(input).replaceAll("").trim();
    }

	public static long parseDateDiff(String time, boolean future) throws Exception {
		Matcher m = timePattern.matcher(time);
		int years = 0;
		int months = 0;
		int weeks = 0;
		int days = 0;
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		boolean found = false;
		while (m.find()) {
			if (m.group() == null || m.group().isEmpty()) {
				continue;
			}
			for (int i = 0; i < m.groupCount(); i++) {
				if (m.group(i) != null && !m.group(i).isEmpty()) {
					found = true;
					break;
				}
			}
			if (found) {
				if (m.group(1) != null && !m.group(1).isEmpty()) years = Integer.parseInt(m.group(1));
				if (m.group(2) != null && !m.group(2).isEmpty()) months = Integer.parseInt(m.group(2));
				if (m.group(3) != null && !m.group(3).isEmpty()) weeks = Integer.parseInt(m.group(3));
				if (m.group(4) != null && !m.group(4).isEmpty()) days = Integer.parseInt(m.group(4));
				if (m.group(5) != null && !m.group(5).isEmpty()) hours = Integer.parseInt(m.group(5));
				if (m.group(6) != null && !m.group(6).isEmpty()) minutes = Integer.parseInt(m.group(6));
				if (m.group(7) != null && !m.group(7).isEmpty()) seconds = Integer.parseInt(m.group(7));
				break;
			}
		}
		Calendar c = new GregorianCalendar();
		if (years > 0) c.add(Calendar.YEAR, years * (future ? 1 : -1));
		if (months > 0) c.add(Calendar.MONTH, months * (future ? 1 : -1));
		if (weeks > 0) c.add(Calendar.WEEK_OF_YEAR, weeks * (future ? 1 : -1));
		if (days > 0) c.add(Calendar.DAY_OF_MONTH, days * (future ? 1 : -1));
		if (hours > 0) c.add(Calendar.HOUR_OF_DAY, hours * (future ? 1 : -1));
		if (minutes > 0) c.add(Calendar.MINUTE, minutes * (future ? 1 : -1));
		if (seconds > 0) c.add(Calendar.SECOND, seconds * (future ? 1 : -1));
		Calendar max = new GregorianCalendar();
		max.add(Calendar.YEAR, 10);
		if (c.after(max)) return max.getTimeInMillis();
		return c.getTimeInMillis();
	}
}
