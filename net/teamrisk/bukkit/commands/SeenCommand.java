package net.teamrisk.bukkit.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.teamrisk.bukkit.ServerTools;
import net.teamrisk.common.UUIDFetcher;

public class SeenCommand implements CommandExecutor {

	private ServerTools plugin;

	public SeenCommand(ServerTools main) {
		this.plugin = main;
		
	}

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You have to be a player to use this command!");
			return false;
		}
		
		Player p = (Player) sender;
		
		if (args.length == 0) {
			sender.sendMessage("Shows the last logout time of a player. \n /seen <playername>");
			return false;
		}

		UUID uuidTarget = null;
		UUID uuidSender = p.getUniqueId();
		try {
			uuidTarget = UUIDFetcher.getUUIDOf(args[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Player player = Bukkit.getPlayer(uuidTarget);

		if (uuidTarget == null) {
			sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
			return false;
		}

		if (player == null) {
			plugin.tcp.sendData("seen," + uuidTarget.toString() + "," + uuidSender.toString() + ",null");
			return false;
		}

		plugin.tcp.sendData("seen," + uuidTarget.toString() + "," + uuidSender.toString() + ",null");

		return false;
	}

}
