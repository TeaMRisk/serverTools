package net.teamrisk.bukkit.commands;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.teamrisk.bukkit.utils.GetLocation;
import net.teamrisk.common.UUIDFetcher;

public class GetLocCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		
		Player player = (Player) sender;
		
		if (args.length == 0) {
			sender.sendMessage(ChatColor.RED + "/location <player>");
			return false;
		}

		String target = args[0];

		UUID uuidS = player.getUniqueId();
		UUID uuidT = null;
		try {
			uuidT = UUIDFetcher.getUUIDOf(target);
		} catch (IOException e) {
			e.printStackTrace();
		}

		GetLocation.requistLocation(uuidT, uuidS);

		return false;
	}

}