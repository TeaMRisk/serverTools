package net.teamrisk.bukkit.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.teamrisk.bukkit.ServerTools;
import net.teamrisk.common.UUIDFetcher;

public class KickCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		
		if (args.length == 0) {
			sender.sendMessage(ChatColor.RED + "/kick <Player> [reason]");
			return false;
		}

		UUID uuidSender;
		if (sender instanceof Player) {
			Player p = (Player) sender;
			uuidSender = p.getUniqueId();
		} else {
            uuidSender = UUID.randomUUID();
        }

		UUID uuidTarget = null;

		try {
			uuidTarget = UUIDFetcher.getUUIDOf(args[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (uuidTarget == null) {
			sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
			return false;
		}

		if (args.length == 1) {
			ServerTools.plugin.tcp
					.sendData("kick," + uuidTarget.toString() + "," + uuidSender.toString() + ",null");
			return false;
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i=1; i<args.length; i++){
		    sb.append(args[i]).append(" ");
		}
		String msg = sb.toString();

		ServerTools.plugin.tcp.sendData("kick," + uuidTarget.toString() + "," + uuidSender.toString() + "," + msg);

		return false;
	}

}
