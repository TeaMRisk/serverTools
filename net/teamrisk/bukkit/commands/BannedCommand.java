package net.teamrisk.bukkit.commands;

import net.md_5.bungee.api.ChatColor;
import net.teamrisk.bukkit.ServerTools;
import net.teamrisk.common.Time;
import net.teamrisk.common.UUIDFetcher;
import org.apache.commons.lang.time.DateUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by John on 1/21/2017.
 */
public class BannedCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {

        //ban <name> <reason> <time>
        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "/ban <name> [reason] [time]");
            return false;
        }

        UUID uuidSender;
        if (sender instanceof Player) {
            Player p = (Player) sender;
            uuidSender = p.getUniqueId();
        } else {
            uuidSender = UUID.randomUUID();
        }

        UUID uuidTarget = null;

        try {
            uuidTarget = UUIDFetcher.getUUIDOf(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (uuidTarget == null) {
            sender.sendMessage(org.bukkit.ChatColor.RED + "Error: " + org.bukkit.ChatColor.DARK_RED + "Player not found.");
            return false;
        }

        if (args.length == 1) {
            ServerTools.plugin.tcp.sendData("banned," + uuidTarget + "," + uuidSender + ",null,-1");
            return true;
        }

        StringBuilder sb = new StringBuilder();
        for(int i=1; i<args.length; i++){
            sb.append(args[i]).append(" ");
        }
        String msg = sb.toString();

        long banTime = -1;
        try {
            banTime = Time.parseDateDiff(msg, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String reason = Time.removeTimePattern(msg);
        System.out.println(banTime);

        ServerTools.plugin.tcp.sendData("banned," + uuidTarget + "," + uuidSender + "," + reason + "," + banTime);

        return false;
    }
}
