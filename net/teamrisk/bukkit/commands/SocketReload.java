package net.teamrisk.bukkit.commands;

import net.teamrisk.bukkit.ServerTools;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import net.teamrisk.bukkit.ClientTCP;

public class SocketReload implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (sender.hasPermission("servertools.socketreload")) {
			ServerTools.plugin.onDisable();
			ServerTools.plugin.onEnable();
		} else {
			sender.sendMessage(ChatColor.DARK_RED + "You don't have permission to excute this command!");
			return false;
		}

		return true;
	}

}
