package net.teamrisk.bukkit.listeners;

import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import net.teamrisk.bukkit.events.DataReceivedEvent;
import net.teamrisk.bukkit.handlers.Kick;
import net.teamrisk.bukkit.handlers.Location;
import net.teamrisk.bukkit.handlers.Seen;

public class OnDataReceived implements Listener {

	@EventHandler
	public void onDataReceived(DataReceivedEvent e) {
		System.out.println(e.getData());
		String[] receivedData = e.getData().split(",");
		String command = receivedData[0];
		String ruuidT = receivedData[1];
		String ruuidS = receivedData[2];
		UUID uuidTarget = UUID.fromString(ruuidT);
		UUID uuidSender = UUID.fromString(ruuidS);
		String data = receivedData[3];
		String back = command + "," + ruuidT + "," + ruuidS + ",";

		if (command.equals("location")) {
			Location.getLocation(receivedData, back, uuidTarget, uuidSender, data);
		}
		if (command.equals("seen")) {
			Seen.getSeen(data, uuidSender, uuidTarget);
		}
		if (command.equals("kick")) {
			Kick.getkick(data, uuidSender);
		}

	}

}
