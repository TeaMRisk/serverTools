package net.teamrisk.bukkit;

import net.teamrisk.bukkit.commands.*;
import org.bukkit.plugin.java.JavaPlugin;

import net.teamrisk.bukkit.listeners.OnDataReceived;
import net.teamrisk.bukkit.utils.PluginMessagingChannel;

public class ServerTools extends JavaPlugin {

	Config config;
	public ClientTCP tcp;
	public static ServerTools plugin;

	public void onEnable() {
		ServerTools.plugin = this;
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord",
				new PluginMessagingChannel());
		this.getServer().getPluginManager().registerEvents(new OnDataReceived(), this);
		this.getCommand("kick").setExecutor(new KickCommand());
		this.getCommand("socketreload").setExecutor(new SocketReload());
		this.getCommand("seen").setExecutor(new SeenCommand(this));
		this.getCommand("location").setExecutor(new GetLocCMD());
		this.getCommand("ban").setExecutor(new BannedCommand());
		config = new Config(this);
		config.setupConfig();
		this.tcp = new ClientTCP();
		tcp.restartConnetion();
	}

	@Override
	public void onDisable() {
		tcp = null;
	}
}