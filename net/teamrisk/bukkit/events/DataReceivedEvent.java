package net.teamrisk.bukkit.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class DataReceivedEvent extends Event {

	String data;

	public DataReceivedEvent(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}

	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
