package net.teamrisk.bukkit;

public class Config {
	
	private ServerTools plugin;
	
	public Config(ServerTools main) {
		this.plugin = main;
	}
	
	public void setupConfig() {
		plugin.getConfig().addDefault("Bungee.name", plugin.getServer().getServerName());
		plugin.getConfig().addDefault("Bungee.host", "localhost");
		plugin.getConfig().addDefault("Bungee.port", 1558);
		
		plugin.getConfig().options().copyDefaults(true);
		plugin.saveConfig();
		
	}
	
}
