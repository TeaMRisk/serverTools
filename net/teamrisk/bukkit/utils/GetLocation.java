package net.teamrisk.bukkit.utils;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class GetLocation extends Utils {

	public static Object getLocation(UUID uuid) {
		Player player = Bukkit.getPlayer(uuid);
		Location loc = player.getLocation();
		String server = player.getServer().getServerName();
		World world = loc.getWorld();
		int X = loc.getBlockX();
		int Y = loc.getBlockY();
		int Z = loc.getBlockZ();
		Object obj = server + "," + world.getName() + "," + X + "," + Y + "," + Z;
		return obj;
	}

	public static void requistLocation(UUID uuidT, UUID uuidS) {
		getPlugin().tcp.sendData("location," + uuidT.toString() + "," + uuidS.toString() + ",null");
	}
	
	public static void receivedLocation(UUID uuid, String data) {
		
		Player player = Bukkit.getPlayer(uuid);
		
		player.sendMessage(data);

	}

}
