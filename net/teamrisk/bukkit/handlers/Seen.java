package net.teamrisk.bukkit.handlers;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import net.teamrisk.bukkit.utils.GetLocation;

public class Seen {

	public static void getSeen(String data, UUID uuidSender, UUID uuidTarget) {

		Player player = Bukkit.getPlayer(uuidSender);
		String[] msg = data.split("@");
		String data1 = msg[0];
		String ip = null;
		if (msg.length >= 1) {
			ip = msg[1];
		}
		
		for (int i = 0; i < msg.length; i++) {
		    System.out.println(String.format("[%d]: %s", i, msg[i]));
		}
		
		if (player.hasPermission("servertools.seen")) {
			player.sendMessage(data1);
			if (player.hasPermission("servertools.seen.ip")) {
				player.sendMessage(ip);
			}
			if (player.hasPermission("servertools.seen.location")) {
				GetLocation.requistLocation(uuidTarget, uuidSender);
			}
		} else {
			player.sendMessage(ChatColor.DARK_RED + "You do not have access to that command.");
		}
		
	}

}
