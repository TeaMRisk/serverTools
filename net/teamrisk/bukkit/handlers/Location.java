package net.teamrisk.bukkit.handlers;

import java.util.UUID;

import org.bukkit.ChatColor;

import net.teamrisk.bukkit.ServerTools;
import net.teamrisk.bukkit.utils.GetLocation;

public class Location {

	public static void getLocation(String[] receivedData, String back, UUID uuidTarget, UUID uuidSender, String data) {
		if (data.equals("null")) {
			ServerTools.plugin.tcp.sendData(back + GetLocation.getLocation(uuidTarget));
		} else {
			String server = receivedData[3];
			String world = receivedData[4];
			String x = receivedData[5];
			String y = receivedData[6];
			String z = receivedData[7];
			String loc = ChatColor.GOLD + " - Location: " + ChatColor.RESET + "(Server: " + server + " W: " + world + ", " + x + ", " + y + ", " + z + ")";
			GetLocation.receivedLocation(uuidSender, loc);
		}
	}

}
