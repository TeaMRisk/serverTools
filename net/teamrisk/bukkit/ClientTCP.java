package net.teamrisk.bukkit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.bukkit.Bukkit;

import net.teamrisk.bukkit.events.DataReceivedEvent;

public class ClientTCP {

	private Socket socket;
	private PrintWriter writer;
	private BufferedReader reader;
	Thread thread;
	private boolean start;
	
	public void TCP() {
		try {
			this.socket = new Socket(ServerTools.plugin.getConfig().getString("Bungee.host"), ServerTools.plugin.getConfig().getInt("Bungee.port"));
			System.out.println(writer);
			System.out.println(reader);
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new PrintWriter(socket.getOutputStream(), true);
			writer.println(Bukkit.getServer().getServerName());
			start = true;

		} catch (IOException e) {
			System.out.println("[serverTools] Cannot Connect to Bungee Server.");
			start = false;
			return;
		}

		thread = new Thread() {
			@Override
			public void run() {
				receiveData();
			}
		};
		
	}

	private void receiveData() {
		try {
			while (true) {
				Bukkit.getPluginManager().callEvent(new DataReceivedEvent(reader.readLine()));
			}
		} catch (IOException e) {
			System.out.println("[serverTools] Lost Connection to Bungee Server.");
			return;
		}
	}
	
	public void restartConnetion() {
		System.out.println(socket);
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		TCP();
		if (start) {
			if (thread != null) {
				thread.interrupt();
			}
			thread.start();
		}
	}

	public void sendData(Object data) {
		writer.println(data);
	}

}
