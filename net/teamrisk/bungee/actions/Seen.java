package net.teamrisk.bungee.actions;

import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.teamrisk.bungee.BungeeMain;
import net.teamrisk.bungee.ServerTCP;
import net.teamrisk.common.Time;

public class Seen {

	public static void getSeen(String data, UUID uuidTarget, UUID uuidSender, String getData, String back) {
		ProxiedPlayer player = BungeeMain.plugin.getProxy().getPlayer(uuidSender);
		Server serverS = player.getServer();
		if (player != null && player.isConnected()) {
			ServerTCP.send(serverS.getInfo().getName(), back + getInfo(uuidTarget));
		}
		
		System.out.println("gg" + serverS.getInfo().getName());
	}

	private static String getInfo(UUID uuid) {

		ProxiedPlayer player = BungeeMain.plugin.getProxy().getPlayer(uuid);

		System.out.println(player);

		if (player == null && BungeeMain.plugin.playerFiles.neverPlayed(uuid)) {
			return ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.";
		}

		BungeeMain.plugin.playerFiles.loadPlayerFile(uuid);

		if (player != null && player.isConnected()) {
			String msg = ChatColor.GOLD + "Player " + ChatColor.RED + player.getName() + ChatColor.GOLD + " has been "
					+ ChatColor.GREEN + "online" + ChatColor.GOLD + " since "
					+ Time.getOnlineTime(BungeeMain.plugin.playersConfig.getLong("timestamps.login")) + ChatColor.GOLD
					+ "." + "@" + ChatColor.GOLD + " - IP Address: " + ChatColor.RESET
					+ BungeeMain.plugin.playersConfig.getString("player.ip");
			return msg;
		}

		String msg = ChatColor.GOLD + "Player " + ChatColor.RED
				+ BungeeMain.plugin.playersConfig.getString("player.name") + ChatColor.GOLD + " has been "
				+ ChatColor.DARK_RED + "offline" + ChatColor.GOLD + " since "
				+ Time.getOnlineTime(BungeeMain.plugin.playersConfig.getLong("timestamps.logout")) + ChatColor.GOLD
				+ "." + "@" + ChatColor.GOLD + " - IP Address: " + ChatColor.RESET
				+ BungeeMain.plugin.playersConfig.getString("player.ip");
		return msg;

	}

}
