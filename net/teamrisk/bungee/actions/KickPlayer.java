package net.teamrisk.bungee.actions;

import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.teamrisk.bungee.BungeeMain;
import net.teamrisk.bungee.ServerTCP;

public class KickPlayer {
	
	public static void Kick(String data, UUID uuidTarget, UUID uuidSender, String back) {
		
		ProxiedPlayer player = BungeeMain.plugin.getProxy().getPlayer(uuidSender);
		if (player == null) {
            if (data.equals("null")) {
                kickP(uuidTarget,"You have been kick from the server!");
            } else {
                kickP(uuidTarget, data);
            }
            return;
        }
		Server serverS = player.getServer();
		if (data.equals("null")) {
			ServerTCP.send(serverS.getInfo().getName(), back + kickP(uuidTarget,"You have been kick from the server!"));
		} else {
			ServerTCP.send(serverS.getInfo().getName(), back + kickP(uuidTarget, data));
		}
	}

	public static String kickP(UUID uuid, String msg) {

		ProxiedPlayer player = BungeeMain.plugin.getProxy().getPlayer(uuid);

		if (player == null) {
			return "player is offline";
		}

		TextComponent message = new TextComponent(ChatColor.RED + msg);

		player.disconnect(message);
		System.out.println(player + " was kicked from server");
		
		return player + " was kicked from server";
	}

}
