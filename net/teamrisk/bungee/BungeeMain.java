package net.teamrisk.bungee;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.teamrisk.bungee.listeners.OnPlayerJoin;
import net.teamrisk.bungee.listeners.OnPlayerLeave;
import net.teamrisk.bungee.listeners.OnServerReceiveData;
import net.teamrisk.bungee.utils.BanList;
import net.teamrisk.bungee.utils.BungeeConfig;
import net.teamrisk.bungee.utils.PlayerFiles;

public class BungeeMain extends Plugin {
	
	public static BungeeMain plugin;
	public Configuration configuration;
	public BungeeConfig config;
	public Configuration banListConfig;
	public Configuration banIPConfig;
	public BanList banList;
	public Configuration playersConfig;
	public PlayerFiles playerFiles;
	public ServerTCP tcp;

	public void onEnable() {
		BungeeMain.plugin = this;
		config = new BungeeConfig(this);
		banList = new BanList(this);
		playerFiles = new PlayerFiles(this);
		config.setupConfig();
		config.saveDefaultConfig();
		banList.setupBanList();
		playerFiles.setupPlayerFolder();
		config.loadConfig();
		setupListeners();
		new Thread() {
			public void run() {
				tcp = new ServerTCP(configuration.getInt("Socket.port"));
			}
		}.start();
		

	}

	public void setupListeners() {
		getProxy().getPluginManager().registerListener(this, new OnServerReceiveData());
		getProxy().getPluginManager().registerListener(this, new OnPlayerJoin(this));
		getProxy().getPluginManager().registerListener(this, new OnPlayerLeave(this));
	}

}