package net.teamrisk.bungee.listeners;

import java.util.UUID;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.teamrisk.bungee.BungeeMain;

public class OnPlayerLeave implements Listener {
	
	private BungeeMain plugin;
	
	public OnPlayerLeave(BungeeMain main) {
		this.plugin = main;
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerDisconnectEvent e) {

		ProxiedPlayer player = e.getPlayer();
		UUID uuid = player.getUniqueId();
		
		plugin.playerFiles.loadPlayerFile(uuid);
		plugin.playersConfig.set("timestamps.logout", System.currentTimeMillis());
		plugin.playerFiles.savePlayerFile(uuid);
	}

}
