package net.teamrisk.bungee.listeners;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.teamrisk.bungee.BungeeMain;
import net.teamrisk.bungee.utils.BannedUtil;
import net.teamrisk.bungee.utils.PlayerFiles;
import net.teamrisk.common.UUIDFetcher;

import java.io.IOException;
import java.util.UUID;

public class OnPlayerJoin implements Listener {

    private BungeeMain plugin;
    private PlayerFiles playerFiles;

    public OnPlayerJoin(BungeeMain main) {
        playerFiles = new PlayerFiles(main);
        this.plugin = main;
    }

    @EventHandler
    public void onPlayerJoin(PostLoginEvent e) {
        ProxiedPlayer player = e.getPlayer();
        UUID uuid = player.getUniqueId();
        String ip = player.getAddress().getAddress().getHostAddress();

        playerFiles.isFirstTime(uuid);
        playerFiles.loadPlayerFile(uuid);
        plugin.playersConfig.set("player.name", player.getName());
        plugin.playersConfig.set("player.ip", ip);
        plugin.playersConfig.set("timestamps.login", System.currentTimeMillis());

        playerFiles.savePlayerFile(uuid);

        System.out.println("Player " + player.getName() + " logged in with UUID=" + uuid);
    }

    @EventHandler
    public void onPlayerConnect(PreLoginEvent e) {

        TextComponent nullUUID = new TextComponent("You Don't have a vaild UUID to connect to this server!");
        nullUUID.setColor(ChatColor.RED);

        UUID uuid = null;
        try {
            uuid = UUIDFetcher.getUUIDOf(e.getConnection().getName());
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if (uuid == null) {
            e.setCancelReason(nullUUID);
            e.setCancelled(true);
        }

        if (BannedUtil.isIPBanned(e.getConnection().getAddress().getAddress().getHostAddress())) {
            e.setCancelReason(BannedUtil.reason(e.getConnection().getAddress().getAddress()));
            e.setCancelled(true);
        }

        if (BannedUtil.isBanned(uuid)) {
            e.setCancelReason(BannedUtil.reason(uuid));
            e.setCancelled(true);
        }
    }

}
