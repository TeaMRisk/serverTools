package net.teamrisk.bungee.listeners;

import java.util.UUID;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.teamrisk.bungee.BungeeMain;
import net.teamrisk.bungee.actions.KickPlayer;
import net.teamrisk.bungee.actions.Location;
import net.teamrisk.bungee.actions.Seen;
import net.teamrisk.bungee.events.DataReceivedEvent;
import net.teamrisk.bungee.utils.BannedUtil;

public class OnServerReceiveData implements Listener {

	@EventHandler
	public void onServerReceiveData(DataReceivedEvent e) {
		String getData = e.getData();
		String[] receivedData = e.getData().split(",");
		String command = receivedData[0];
		String ruuidT = receivedData[1];
		String ruuidS = receivedData[2];
		UUID uuidTarget = UUID.fromString(ruuidT);
		UUID uuidSender = UUID.fromString(ruuidS);
		String data = receivedData[3];
		String back = command + "," + ruuidT + "," + ruuidS + ",";
		
		System.out.println(e.getData());
		
		if (command.equals("location")) {
			Location.getLocation(data, uuidTarget, uuidSender, getData);
		}
		if (command.equals("seen")) {
			Seen.getSeen(data, uuidTarget, uuidSender, getData, back);
		}
		if (command.equals("kick")) {
			KickPlayer.Kick(data, uuidTarget, uuidSender, back);
		}
		if (command.equals("banned")) {
			BannedUtil.banPlayer(uuidSender, uuidTarget, data, receivedData[4]);
		}

		

	}

}
