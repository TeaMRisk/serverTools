package net.teamrisk.bungee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import net.md_5.bungee.api.ProxyServer;
import net.teamrisk.bungee.events.DataReceivedEvent;

public class ServerTCP extends Thread {

	private ServerSocket serverSocket;
	private static HashMap<String, Socket> clientsList = new HashMap<String, Socket>();
	private String recived;
	
	private TCP tcp;
	

	public ServerTCP(int serverPort) {
		try {
			serverSocket = new ServerSocket(serverPort);
			while (true) {
				Socket socket = serverSocket.accept();
				System.out.println("Connected: " + socket.getLocalPort());
				tcp = new TCP(socket);
				tcp.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getRecived() {
		return recived;
	}

	static class TCP extends Thread {
		private Socket socket;
		private BufferedReader reader;
		private String sender;

		public TCP(Socket socket) {
			this.socket = socket;
			try {
				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				sender = reader.readLine();
				clientsList.put(sender, socket);
				System.out.println(clientsList);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			while (true) {
				try {
					ProxyServer.getInstance().getPluginManager().callEvent(new DataReceivedEvent(reader.readLine(), sender));
				} catch (IOException ex) {
					System.out.println(socket.getPort() + " disconnected");
					return;
				}
			}
		}
	}

	public static void send(String server, String msg) {
		Socket socket = clientsList.get(server);
		try {
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			writer.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void sendToAll(String data) {
		for (Socket socket : clientsList.values()) {
			try {
				PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
				writer.println(data);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	


}
