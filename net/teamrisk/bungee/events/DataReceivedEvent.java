package net.teamrisk.bungee.events;

import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

public class DataReceivedEvent extends Event implements Cancellable{
	
	String data;
	String sender;
	private boolean cancelled;
	
	public DataReceivedEvent(String data, String sender) {
		this.data = data;
		this.sender = sender;
	}
	
	public String getData() {
		return data;
	}
	
	public String getSender() {
		return sender;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		this.cancelled = arg0;
	}
	
	
}
