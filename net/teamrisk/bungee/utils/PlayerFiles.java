package net.teamrisk.bungee.utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.teamrisk.bungee.BungeeMain;

public class PlayerFiles {
	
	private BungeeMain plugin;

	public PlayerFiles(BungeeMain main) {
		this.plugin = main;
	}
	
	public void setupPlayerFolder() {
		File playersFolder = new File(plugin.getDataFolder() + "/players/");
		
		if (!playersFolder.exists()) {
			playersFolder.mkdirs();
		}
	}
	
	public boolean isFirstTime(UUID uuid) {
		
		ProxiedPlayer player = plugin.getProxy().getPlayer(uuid);
		
		File playerFile = new File(plugin.getDataFolder() + "/players/" + uuid.toString() + ".yml");
		
		if (!playerFile.exists()) {
			try {
				System.out.println("Player file " + uuid.toString() + " doesn't exist creating one...");
				playerFile.createNewFile();
			} catch (IOException e) {
				System.out.println("Failed to create player file for player " + player.getName());
				e.printStackTrace();
			}
			return true;
		}
		
		return false;
	}
	
	public boolean neverPlayed(UUID uuid) {
		
		File playerFile = new File(plugin.getDataFolder() + "/players/" + uuid.toString() + ".yml");

        return !playerFile.exists();

    }
	
	public void savePlayerFile(UUID uuid) {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(plugin.playersConfig,
					new File(plugin.getDataFolder() + "/players/", uuid.toString() + ".yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadPlayerFile(UUID uuid) {
		try {
			plugin.playersConfig = ConfigurationProvider.getProvider(YamlConfiguration.class)
					.load(new File(plugin.getDataFolder() + "/players/", uuid.toString() + ".yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
