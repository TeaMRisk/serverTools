package net.teamrisk.bungee.utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.config.Configuration;
import net.teamrisk.bungee.BungeeMain;
import net.teamrisk.common.Time;
import net.teamrisk.common.TimeBetween;
import net.teamrisk.common.UUIDFetcher;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by John on 1/17/2017.
 */
public class BannedUtil {

    public static Boolean isBanned(UUID uuid){
        BungeeMain.plugin.banList.loadBanList();
        if (BungeeMain.plugin.banListConfig.get(uuid.toString()) != null) {
            String expires = BungeeMain.plugin.banListConfig.getString(uuid + ".expires");
            if (expires.equalsIgnoreCase("forever")) {
                return true;
            }
            if (TimeBetween.getString(Time.fromString(LocalDateTime.now().toLocalDate()
                    + " " + LocalDateTime.now().withNano(0).toLocalTime()), Time.fromString(expires)).startsWith("-")) {
                BungeeMain.plugin.banListConfig.set(uuid.toString(), null);
                BungeeMain.plugin.banList.saveBanList();
                return false;
            }
            return true;
        }
        return false;
    }

    public static Boolean isIPBanned(String IPAddress) {
        BungeeMain.plugin.banList.loadBanIPS();
        return BungeeMain.plugin.banIPConfig.get(IPAddress.replace('.', '-')) != null;
    }

    public static TextComponent reason(UUID uuid) {
        BungeeMain.plugin.banList.loadBanList();
        String expires = BungeeMain.plugin.banListConfig.getString(uuid + ".expires");
        String forever = "";
        if (expires.equalsIgnoreCase("forever")) forever = "permanently ";
        TextComponent reason = new TextComponent();
        // You have been banned at: Time by: name until: expires for: reason time left: timeleft
        reason.addExtra("You have been " + forever + "banned at: " + ChatColor.GOLD + BungeeMain.plugin.banListConfig.getString(uuid + ".created") + "\n");
        reason.addExtra("by: " + ChatColor.GOLD + BungeeMain.plugin.banListConfig.getString(uuid + ".source") + "\n");
        if (!expires.equalsIgnoreCase("forever")) reason.addExtra("until: " + ChatColor.GOLD + expires + "\n");
        reason.addExtra("reason: " + ChatColor.GOLD + ChatColor.translateAlternateColorCodes(
                '&', BungeeMain.plugin.banListConfig.getString(uuid.toString() + ".reason")) + "\n");
        if (!expires.equalsIgnoreCase("forever")) reason.addExtra("time until unbanned: " + ChatColor.GOLD + TimeBetween.getString(
                Time.fromString(LocalDateTime.now().toLocalDate() + " " + LocalDateTime.now().withNano(0).toLocalTime()), Time.fromString(expires)));
        reason.setColor(ChatColor.RED);
        return reason;
    }

    public static TextComponent reason(InetAddress IPAddress) {
        BungeeMain.plugin.banList.loadBanIPS();
        String ip = IPAddress.getHostAddress().replace('.','-');
        String expires = BungeeMain.plugin.banIPConfig.getString( ip + ".expires");
        String forever = "";
        if (expires.equalsIgnoreCase("forever")) forever = "permanently ";
        TextComponent reason = new TextComponent();
        reason.addExtra("You have been " + forever + "IP banned at: " + ChatColor.GOLD + BungeeMain.plugin.banIPConfig.getString(ip + ".created") + "\n");
        reason.addExtra("by: " + ChatColor.GOLD + BungeeMain.plugin.banIPConfig.getString(ip + ".source") + "\n");
        if (!expires.equalsIgnoreCase("forever")) reason.addExtra("until: " + ChatColor.GOLD + expires + "\n");
        reason.addExtra("reason: " + ChatColor.GOLD + ChatColor.translateAlternateColorCodes('&', BungeeMain.plugin.banIPConfig.getString(ip + ".reason")) + "\n");
        if (!expires.equalsIgnoreCase("forever")) reason.addExtra("time until unbanned: " + ChatColor.GOLD + TimeBetween.getString(
                Time.fromString(LocalDateTime.now().toLocalDate() + " " + LocalDateTime.now().withNano(0).toLocalTime()), Time.fromString(expires)));
        reason.setColor(ChatColor.RED);
        return reason;
    }

    public static void banPlayer(UUID senderUUID, UUID targetUUID, String reason, String btime) {

        String sender = ProxyServer.getInstance().getPlayer(senderUUID).getName();
        String uuid = targetUUID.toString();

        System.out.println(btime);
        long time = Long.parseLong(btime);
        System.out.println(time);
        BungeeMain.plugin.banList.loadBanList();
        Configuration banList = BungeeMain.plugin.banListConfig;
        banList.set(uuid + ".created", Time.getCurrentTime());
        banList.set(uuid + ".source", sender);
        if (time == -1) {
            banList.set(uuid + ".expires", "forever");
        } else {
            banList.set(uuid + ".expires", Time.fromMilliseconds(time));
        }

        TextComponent reasonComponent = new TextComponent(reason);
        if (reason.equalsIgnoreCase("null")) {
            reasonComponent = new TextComponent("The Ban Hammer has spoken!");
            banList.set(uuid + ".reason", reasonComponent.getText());
        } else {
            banList.set(uuid + ".reason", reasonComponent.getText());
        }

        BungeeMain.plugin.banList.saveBanList();

        if (ProxyServer.getInstance().getPlayer(targetUUID) != null) {
            ProxyServer.getInstance().getPlayer(targetUUID).disconnect(BannedUtil.reason(targetUUID));
        }

    }

}
