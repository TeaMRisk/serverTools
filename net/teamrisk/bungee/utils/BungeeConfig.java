package net.teamrisk.bungee.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.io.ByteStreams;

import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.teamrisk.bungee.BungeeMain;

public class BungeeConfig {

	private BungeeMain plugin;

	public BungeeConfig(BungeeMain main) {
		this.plugin = main;
	}

	public void setupConfig() {
		if (!plugin.getDataFolder().exists())
			plugin.getDataFolder().mkdir();
		File configFile = new File(plugin.getDataFolder(), "config.yml");

		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				try (InputStream is = plugin.getResourceAsStream("config.yml");
						OutputStream os = new FileOutputStream(configFile)) {
					//ByteStreams.copy(is, os);
				}
			} catch (IOException e) {
				throw new RuntimeException("Unable to create configuration file", e);
			}
		}

		try {
			plugin.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void saveDefaultConfig() {
		loadConfig();
		if (plugin.configuration.get("Socket.port") != null)
			return;
		plugin.configuration.set("Socket.port", 1558);
		saveConfig();
	}

	public void saveConfig() {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(plugin.configuration,
					new File(plugin.getDataFolder(), "config.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadConfig() {
		try {
			plugin.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class)
					.load(new File(plugin.getDataFolder(), "config.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
