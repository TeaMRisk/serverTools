package net.teamrisk.bungee.utils;

import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.teamrisk.bungee.BungeeMain;

import java.io.*;

public class BanList {

	private BungeeMain plugin;

	public BanList(BungeeMain main) {
		this.plugin = main;
	}

	public void setupBanList() {
		if (!plugin.getDataFolder().exists())
			plugin.getDataFolder().mkdir();
		File bannedPlayers = new File(plugin.getDataFolder(), "../../banned-players.yml");
		File bannedIPS = new File(plugin.getDataFolder(), "../../banned-ips.yml");

		createFile(bannedPlayers, "../../banned-players.yml");
		createFile(bannedIPS, "../../banned-ips.yml");

		try {
			plugin.banListConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(bannedPlayers);
			plugin.banIPConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(bannedIPS);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void createFile(File file, String name) {
        if (!file.exists()) {
            try {
                file.createNewFile();
                try (InputStream is = plugin.getResourceAsStream(name);
                     OutputStream os = new FileOutputStream(file)) {
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create banlist file", e);
            }
        }
    }

	public void saveBanList() {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(plugin.banListConfig,
					new File(plugin.getDataFolder(), "../../banned-players.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveBanIPS() {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(plugin.banIPConfig,
					new File(plugin.getDataFolder(), "../../banned-ips.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadBanList() {
		try {
			plugin.banListConfig = ConfigurationProvider.getProvider(YamlConfiguration.class)
					.load(new File(plugin.getDataFolder(), "../../banned-players.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadBanIPS() {
		try {
			plugin.banIPConfig = ConfigurationProvider.getProvider(YamlConfiguration.class)
					.load(new File(plugin.getDataFolder(), "../../banned-ips.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
